<?php

/**
 * @file
 * Kanooh config module drush integration.
 */

/**
 * Implements hook_drush_command().
 */
function kanooh_config_drush_command() {
  $items = array();

  $items['kanooh-config'] = array(
    'description' => "List all the kanooh configurations for your site.",
    'drupal dependencies' => array('kanooh_config'),
    'aliases' => array('kc'),
    'options' => array(
      'type' => 'The resource specific configuration (e.g. databases, solr)',
    ),
    'examples' => array(
      'drush kc solr --type=solr' => 'Return the solr configuration set by Kanooh environment variables.',
    ),
  );

  return $items;
}

/**
 * Implements drush_COMMAND().
 */
function drush_kanooh_config() {
  // Get global configuration.
  $config = _kanooh_config();

  if (empty($config)) {
    return drush_set_error('KANOOH_CONFIG_UNAVAILABLE', dt('No Kanooh configuration is available.'));
  }

  if ($type = drush_get_option('type')) {
    if (!empty($config[$type])) {
      drush_print(dt('Kanooh configuration for type: %type', array('%type' => $type)) . "\n");
      $config = $config[$type];
    }
    else {
      return drush_set_error('KANOOH_CONFIG_UNAVAILABLE', dt('No Kanooh configuration is available for type %type', array('%type' => $type)));
    }
  }
  else {
    drush_print(dt('Kanooh configuration for all types:') . "\n");
  }

  drush_print(print_r($config, TRUE) . "\n");
}
